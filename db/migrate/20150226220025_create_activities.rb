class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
    	t.string :name 
    	t.integer :reps
    	t.float :distance
    	t.text :meal
    	t.text :post_meal
    	t.boolean :yoga
    	t.integer :duration
    	t.references :user
    	t.date :created_at
    end
  end
end
