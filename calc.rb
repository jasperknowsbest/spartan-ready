
puts "What is your weight in pounds?"
kg = gets.chomp.to_i / 2.20462262185

puts "How tall are you (in inches)?"
cm = gets.chomp.to_i * 2.54


puts "What is your age?"
age = gets.chomp.to_i

old_bmr = (10 * kg) + (6.25 * cm) - (5 * age)

puts "What is your gender (Male or Female)?"
gender = gets.chomp.capitalize


if gender == "Female"
  bmr = old_bmr - 161
 
else gender == "Male"
  bmr = old_bmr + 5
end

print   <<-fit
Indicate your fitness level. Enter 1 - 5:
 1. If you are sedentary (little or no exercise) : Calorie-Calculation = BMR x 1.2
 2. If you are lightly active (light exercise/sports 1-3 days/week) : Calorie-Calculation = BMR x 1.375
 3. If you are moderatetely active (moderate exercise/sports 3-5 days/week) : Calorie-Calculation = BMR x 1.55
 4. If you are very active (hard exercise/sports 6-7 days a week) : Calorie-Calculation = BMR x 1.725
 5 If you are extra active (very hard exercise/sports & physical job or 2x training) : Calorie-Calculation = BMR x 1.9
fit

print "What is your fitness level? "
fit_level = gets.chomp.to_i

if fit_level == 1
	tdee = bmr * 1.2  
elsif fit_level == 2
	tdee = bmr * 1.375 
elsif fit_level == 3
	tdee = bmr * 1.55
elsif fit_level == 4
	tdee = bmr * 1.725
elsif fit_level == 5
	tdee = bmr * 1.9 
else
	p "Please enter a valid number"
end

puts "You're TDEE is #{tdee}"

print   <<-goal
Let's talk goals. Enter 1 - 5:
1. I want to lose weight...AND FAST!! I don't mind loosing a little muscle.
2. I want to lose weight gradually and keep as much muscle on as I can!
3. I'm loving how I look and feel! Let's maintain!
4. I want to put on some more muscle and keep my fat gain to a minimum!
5. I want to put on mass quick! I don't mind a little more fat!
goal

print "What is your goal? "
goal = gets.chomp.to_i

if goal == 1
  new_tdee = tdee * 0.80
elsif goal == 2
  new_tdee = tdee * 0.85
elsif goal == 3
  new_tdee = tdee 
elsif goal == 4
  new_tdee = tdee * 1.05
elsif goal == 5
  new_tdee = tdee * 1.10 
else
  p "Please enter a valid number"
end

puts "You need to take in #{new_tdee.to_i} calories a day to for at least 8 weeks reach your goals!"
puts ""
puts ""

print   <<-body_type
Let's talk goals. Enter 1 - 3:
1. Ectomorph: You are naturally thin with skinny limbs and a high tolerance for carbohydrates. Usually, your metabolic rate is fast. 
2. Mesomorph: You are naturally muscular and athletic. You have a moderate carbohydrate tolerance and a moderate metabolic rate.
3. Endomorph: You are naturally broad and thick.  You have a low carbohydrate tolerance and a slow metabolic rate.
body_type

print "What is your body type? "
body_type = gets.chomp.to_i

if body_type == 1
  carbs = ((new_tdee * 0.55) / 4).to_i
  protein = ((new_tdee * 0.25) / 4).to_i
  fats = ((new_tdee * 0.20) / 9).to_i
elsif body_type == 2
  carbs = ((new_tdee * 0.40) / 4).to_i
  protein = ((new_tdee * 0.30) / 4).to_i
  fats = ((new_tdee * 0.30) / 9).to_i
elsif body_type == 3
  carbs = ((new_tdee * 0.25) / 4).to_i
  protein = ((new_tdee * 0.35) / 4).to_i
  fats = ((new_tdee * 0.40) / 9).to_i 
end

puts "Out of the #{new_tdee.to_i} calories you need to maintain #{carbs} grams of carbohydrates, #{protein} grams of protein and #{fats} grams of fat"

puts "How many meals do you eat a day? Enter 3 - 6"
meals = gets.chomp.to_i

puts "Your ratio per meal:"
puts "Carbs: #{carbs/ meals} grams per meal"
puts "Protein: #{protein/ meals} grams per meal"
puts "Fats: #{fats/ meals} grams per meal"
