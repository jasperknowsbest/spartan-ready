class ActivitiesController < ApplicationController

  def create
  	@user = User.find(params[:user_id])
		@activity = @user.activities.create(params[:activity].permit(:name, :reps, :yoga, :distance, :duration, :meal, :post_meal))
		redirect_to @user
  end

  
end
