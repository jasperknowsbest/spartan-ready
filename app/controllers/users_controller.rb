class UsersController < ApplicationController
	before_action :find_user, only: [:show, :edit, :update, :destroy]


  def show
    @day = Date.today.strftime('%A')
    @done_squat = @user.done("Squat")
    @done_pushup = @user.done("Pushup")
    @done_cardio = @user.done("Cardio")
    @done_plank = @user.done("Plank")
    @done_yoga = @user.done("Yoga")
    @done_burpee = @user.done("Burpee")
    @done_lunch = @user.done("Lunch")
  end

  def welcome
    if user_signed_in?
      redirect_to user_path(current_user)
    end
  end





  private

  def user_params
  	params.require(:user).permit(:name)
  end

  def find_user
		@user = User.find_by(params[:id])
	end
end
