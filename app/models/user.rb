class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
	has_many :activities 

	def done(activity)
		self.activities.exists?(name: activity, created_at: Date.today.strftime("%Y-%m-%d"))
	end

end
