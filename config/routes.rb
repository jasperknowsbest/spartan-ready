Rails.application.routes.draw do

  root 'users#welcome'

  # devise_for :users, :controllers => {:registrations => "users/registrations"} do
  #   get '/users/sign_out' => 'devise/sessions#destroy'
  # end

  get '/users/sign_out' => 'devise/sessions#destroy'

  devise_for :users
  
  resources :users do
    resources :activities
  end

end
